PDG Cooperation Simulator
=============================

Introduction
------------

This a source file for [NetLogo environment](https://ccl.northwestern.edu/netlogo/).
It is able to simulate the impact of cooperation and management strategies on stress and economic performance.

Installation
------------

It is not necessary to install this module, however you need either install desktop
[NetLogo environment](https://ccl.northwestern.edu/netlogo/download.shtml) on your
computer or use [NetLogo Web](http://netlogoweb.org/launch#http://netlogoweb.org/) to run the
simulation.

You can download the project files using download button (right upper part).
After unziping downloaded file, please use the
[NetLogo environment](https://ccl.northwestern.edu/netlogo/download.shtml) to open the
pdg-cooperation-simulator.nlogo file or
upload it to the [NetLogo Web](http://netlogoweb.org/launch#http://netlogoweb.org/).

Version
-------
- v1.0 (2018/09/18) - Initial version with basic functionality.